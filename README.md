
# Hello Sender Game

[![pipeline status](https://gitlab.inria.fr/openvibe/unity-games/hello-sender/badges/master/pipeline.svg)](https://gitlab.inria.fr/openvibe/unity-games/hello-sender/pipelines)
[![Documentation](https://img.shields.io/badge/Documentation-deploy-success)](https://openvibe.gitlabpages.inria.fr/unity-games/hello-sender/)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)  
[![Quality Gate Status](https://sonarqube.inria.fr/sonarqube/api/project_badges/measure?project=inria%3Aopenvibe%3Agames%3Ahellosender%3Amaster&metric=alert_status)](https://sonarqube.inria.fr/sonarqube/dashboard?id=inria%3Aopenvibe%3Agames%3Ahellosender%3Amaster)
[![Bugs](https://sonarqube.inria.fr/sonarqube/api/project_badges/measure?project=inria%3Aopenvibe%3Agames%3Ahellosender%3Amaster&metric=bugs)](https://sonarqube.inria.fr/sonarqube/dashboard?id=inria%3Aopenvibe%3Agames%3Ahellosender%3Amaster)
[![Vulnerabilities](https://sonarqube.inria.fr/sonarqube/api/project_badges/measure?project=inria%3Aopenvibe%3Agames%3Ahellosender%3Amaster&metric=vulnerabilities)](https://sonarqube.inria.fr/sonarqube/dashboard?id=inria%3Aopenvibe%3Agames%3Ahellosender%3Amaster)
[![Code Smells](https://sonarqube.inria.fr/sonarqube/api/project_badges/measure?project=inria%3Aopenvibe%3Agames%3Ahellosender%3Amaster&metric=code_smells)](https://sonarqube.inria.fr/sonarqube/dashboard?id=inria%3Aopenvibe%3Agames%3Ahellosender%3Amaster)

## Description

This game is a template to use LSL to communicate with external software and in our case with [OpenViBE](http://openvibe.inria.fr/).  
In parallel, we create an OpenViBE box with LSL protocol for the game.  
This makes it easier to use the two tools.
In order to make life easier for the community, it will be highly recommended to plan one box per game.
For example, if several streams must be created or not, If several inputs/outputs must be combined, if settings must be given...

## Dependency

### [LSL4Unity](https://gitlab.inria.fr/openvibe/unity-games/LSL4Unity)

The LSL4Unity library used in this project is a fork with OpenViBE specifics.
It is set as a submodule, so you need to pull it with the following command for the project to work:

- git submodule update --init --recursive

## Screenshots

This game shows nothing.

## OpenViBE Scenario Example

<div style="text-align:center"><img src="Documentation/resources/Scenario.png"/></div>
