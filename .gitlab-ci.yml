variables:
  GIT_SUBMODULE_STRATEGY: recursive
  DocumentationPath: _site
  ProjectPath: hello-sender.sln
  BuildsPath: Builds
  PagesPath: public
  BuildsMethod: OVGames.HelloSender.Editor.BuildHooks.BuildAll

stages:
  - prepare
  - build
  - tests
  - documentation
  - analysis
  - deploy

# Shared before_script between jobs
default:
  before_script:
    - UnityVersion=$(grep --only-matching --max-count=1 '[0-9abf.]\+' ProjectSettings/ProjectVersion.txt)
    - Unity="C:/Program\ Files/Unity/Hub/Editor/${UnityVersion}/Editor/Unity.exe"

# Generate csproj files
prepare:
  stage: prepare
  script: eval $Unity -batchmode -quit -projectPath . -executeMethod UnityEditor.SyncVS.SyncSolution
  artifacts:
    paths:
      - "*.csproj"
      - "*.sln"
      - Library

# Build
build:
  stage: build
  variables:
    LogFile: BuildLogs.txt
  script:
    # Build for PC
    - eval $Unity -batchmode -quit -projectPath . -logFile $LogFile -executeMethod $BuildsMethod
    # Job fail if a build failed
    - grep 'result="error CS"' $LogFile && exit 1 || exit 0
  artifacts:
    paths:
      - $BuildsPath
      - $LogFile

# Generate the documentation
documentation:
  stage: documentation
  variables:
    LogFile: DocumentationLogs.txt
  script:
    # Copy and fix README.md links
    - sed -r 's#/?Documentation/##gi' README.md > Documentation/index.md
    # Generate documentation
    - docfx metadata -f Documentation/docfx.json | tee $LogFile
    - docfx build -f Documentation/docfx.json | tee -a $LogFile
    # Job fail if there is at least a warning
    - grep -q "Warning:" $LogFile && exit 1 || exit 0
  allow_failure: true
  artifacts:
    paths:
      - $DocumentationPath
      - $LogFile


# Analyse code quality
analysis:
  stage: analysis
  # Create a SonarQube project for each branch <team>:<project>:<branch>
  # Push tags to <team>:<project>:master with a version number ($CI_COMMIT_TAG is set only when building a tag)
  script:
    - ProjectKey=$([[ -n "$CI_COMMIT_TAG" ]] && echo master || echo "$CI_COMMIT_REF_NAME")
    - ProjectKey="$SonarProjectKey:$ProjectKey"
    - SonarScanner.MSBuild.exe begin
      -k:"$ProjectKey"
      -d:sonar.host.url="$SonarHostUrl"
      -d:sonar.login="$SonarLogin"
      -v:"$CI_COMMIT_TAG"
      -d:sonar.exclusions=Assets/**/Plugins/**/*
    - MsBuild.exe -t:Rebuild $ProjectPath
    - SonarScanner.MSBuild.exe end -d:sonar.login="$SonarLogin"

# Deploy the documentation online
pages:
  stage: deploy
  script: cp -r $DocumentationPath $PagesPath
  artifacts:
    paths:
      - $PagesPath
  only:
    - master

