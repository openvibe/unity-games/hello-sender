﻿///-------------------------------------------------------------------------------------------------
///
/// \file BuildConfig.cs
/// \brief Class For command line Build
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 24/03/2020.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
///
///-------------------------------------------------------------------------------------------------

using System;
using UnityEditor;
using UnityEditor.Build.Reporting;

namespace OVGames.HelloSender.Editor
{
	/// <summary> Wrapper of <see cref="BuildPlayerOptions"/> with improved build path configuration. </summary>
	public sealed class BuildConfig
	{
		/// <summary> Gets or sets the location of the build directory, relative to the project root directory. </summary>
		/// <value> The location of the build directory. </value>
		public string DirectoryPath { get; set; }

		/// <summary> Gets or set the location of the application to build, relative to <see cref="DirectoryPath"/>. </summary>
		/// <value> The location of the application to build, relative to <see cref="DirectoryPath"/>. </value>
		public string RelativeFilePath { get; set; }

		/// <summary> Gets or sets the <see cref="Action"/> to execute after
		/// <see cref="BuildPipeline.BuildPlayer(BuildPlayerOptions)"/> with the <see cref="BuildReport"/>. </summary>
		/// <value> the <see cref="Action"/> to execute after build. </value>
		public Action<BuildReport> AfterBuild { get; set; }

		/// <summary> The build options. </summary>
		/// <value> The build options. </value>
		private BuildPlayerOptions options = new BuildPlayerOptions();

		/// <summary> Gets or sets the options for <see cref="BuildPipeline.BuildPlayer(BuildPlayerOptions)"/>. </summary>
		/// <remarks> <see cref="BuildPlayerOptions.locationPathName"/> is automatically set to
		/// <see cref="DirectoryPath"/> + <see cref="RelativeFilePath"/>. </remarks>
		public BuildPlayerOptions Options
		{
			get
			{
				options.locationPathName = DirectoryPath + RelativeFilePath;
				return options;
			}
			set => options = value;
		}
	}
}
