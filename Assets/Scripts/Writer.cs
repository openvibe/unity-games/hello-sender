﻿///-------------------------------------------------------------------------------------------------
///
/// \file Writer.cs
/// \brief Writer for Log File.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 24/03/2020.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
///
///-------------------------------------------------------------------------------------------------

using System;
using System.IO;
using UnityEngine;

namespace OVGames.HelloSender
{
	/// <summary> Class Used to Write in a Log File </summary>
	public class Writer
	{
		/// <summary> Absolute file path. </summary>
		/// <value> Absolute file path. </value>
		private readonly string path;

		/// <summary> constructor of the class <see cref="Writer"/>. </summary>
		/// <param name="filePath"> Absolute file path. </param>
		public Writer(string filePath = "")
		{
			path = string.IsNullOrEmpty(filePath) ? Application.persistentDataPath + $"/log{DateTime.Now.Hour}h{DateTime.Now.Minute}.txt" : filePath;

			if (File.Exists(path))
			{
				try
				{
					File.Delete(path);
					Debug.Log($"File \"{path}\" is deleted.");
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
					Debug.Log($"Exception \"{e}\" is occured when deleting file.");
					throw;
				}
			}
			else { Debug.Log($"File \"{path}\" is free."); }
		}

		/// <summary> Write a message in file as new line. </summary>
		/// <param name="msg"> message to write (it can contain multiple line). </param>
		public void WriteLine(string msg)
		{
			try
			{
				using (StreamWriter sw = new StreamWriter(path, true))
				{
					sw.WriteLine(msg);
					sw.Close();
				}
				Debug.Log($"Msg \"{msg}\" is saved.");
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				Debug.Log($"Exception \"{e}\" is occured when message is saving.");
				throw;
			}
		}
	}
}
