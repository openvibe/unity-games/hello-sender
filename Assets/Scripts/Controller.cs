﻿///-------------------------------------------------------------------------------------------------
///
/// \file Controller.cs
/// \brief Main Controller to manage the element to send.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 24/03/2020.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
///
///-------------------------------------------------------------------------------------------------

using System;
using LSL4Unity;
using LSL4Unity.OV;
using UnityEngine;

namespace OVGames.HelloSender
{
/// <summary> Controller is used to send some value with LSL. </summary>
/// <seealso cref="UnityEngine.MonoBehaviour" />
public class Controller : MonoBehaviour
{
	// Settings
	[SerializeField] private string signalStream = "ovSignal";
	[SerializeField] private string markerStream = "ovMarker";
	[SerializeField] private int    channelCount = 2;

	// Variables
	private liblsl.StreamOutlet outletSignal, outletMarker;
	private float[]             samples;
	private float               lastTime = 0;
	private Writer              logFile;

	private const int STIMULATION = (int) Stimulations.GDF_BEEP;

	/// <summary> Start is called before the first frame update. </summary>
	void Start()
	{
		samples = new float[channelCount];
		var info = new liblsl.StreamInfo(signalStream, "signal", channelCount, liblsl.IRREGULAR_RATE, liblsl.channel_format_t.cf_float32, "signal");
		outletSignal = new liblsl.StreamOutlet(info);
		Debug.Log($"Creating Stream : Name = {info.Name()}, Type = {info.Type()}, Channel Count = {info.ChannelCount()}, Format = {info.ChannelFormat()}");
		info         = new liblsl.StreamInfo(markerStream, "Marker", 1, liblsl.IRREGULAR_RATE, liblsl.channel_format_t.cf_int32, "stimulation");
		outletMarker = new liblsl.StreamOutlet(info);
		Debug.Log($"Creating Stream : Name = {info.Name()}, Type = {info.Type()}, Channel Count = {info.ChannelCount()}, Format = {info.ChannelFormat()}");
		logFile = new Writer();
	}

	/// <summary> Update is called once per frame. </summary>
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Debug.Log("Quit Game...");
			Application.Quit();
		}

		var t = Time.time;
		for (var i = 0; i < channelCount; ++i) { samples[i] = (float) Math.Sin(t + i); }
		outletSignal.PushSample(samples, liblsl.LocalClock());
		if (!(t - lastTime > 1)) { return; }
		lastTime = t;
		outletMarker.PushSample(new[] { STIMULATION }, liblsl.LocalClock());
		logFile.WriteLine($"{STIMULATION} at {Time.time}");
	}
}
}
